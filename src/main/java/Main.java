import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        final ObjectMapper objectMapper = new ObjectMapper();

        Properties properties = new Properties();
        properties.load(Files.newInputStream(Paths.get("app.properties")));

        final String jiraBaseUrl = Preconditions.checkNotNull(properties.getProperty("jira.base"));
        String username = Preconditions.checkNotNull(properties.getProperty("jira.username"));
        String password = Preconditions.checkNotNull(properties.getProperty("jira.password"));
        String versionName = Preconditions.checkNotNull(properties.getProperty("jira.versionName"));
        String projectKey = Preconditions.checkNotNull(properties.getProperty("jira.projectKey"));

        final HttpClient httpClient = HttpClientBuilder.create()
            .setMaxConnPerRoute(10)
            .setConnectionTimeToLive(10, TimeUnit.MINUTES)
            .build();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Map data = ImmutableMap.builder()
            .put("description", "")
            .put("name", versionName)
            .put("archived", false)
            .put("released", false)
            .put("releaseDate", "2015-10-10")
            .put("project", projectKey)
            .build();
        final String serializedData = objectMapper.writeValueAsString(data);

        final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);

        final CountDownLatch countDownLatch = new CountDownLatch(2);
        Runnable runnable = new Runnable() {
            public void run() {
                HttpPost httpPost = new HttpPost(jiraBaseUrl + "/rest/api/latest/version");
                HttpEntity entity = new StringEntity(serializedData, ContentType.APPLICATION_JSON);
                httpPost.setEntity(entity);
                try {
                    httpPost.setHeader(new BasicScheme().authenticate(credentials, httpPost));
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity responseEntity = httpResponse.getEntity();
                    ContentType contentType = ContentType.get(responseEntity);
                    InputStreamReader inputStreamReader = new InputStreamReader(responseEntity.getContent(), contentType.getCharset());
                    System.out.println(objectMapper.readTree(inputStreamReader));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }
            }
        };
        executorService.submit(runnable);
        executorService.submit(runnable);
        countDownLatch.await();
        executorService.shutdown();
    }
}
