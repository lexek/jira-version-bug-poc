# How to run
To run this program you must have app.properties file in your working directory with these properties set:

* jira.base - jira base URL (i.e. http://localhost:8080)
* jira.username - jira user name for user that able to create versions in selected project
* jira.password - password for that user
* jira.versionName - name for version to create
* jira.projectKey - project key
